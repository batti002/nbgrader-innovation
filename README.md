# nbgrader-innovation

project goals:
using jupyter notebooks and nbgrader to partially automate exercise grading for the course geoengineering, and the design of a proposal for a final exam for the course geoengineering



this is a WIP version to prepare for the workshop

a more detailed version of this that guides you through each of the UI screens in case you get stuck us available here: (subject to upates ofc) 
https://drive.google.com/open?id=1MdjKg3GWRq-u98y06ZZLCBqahFJmjIWps8reR5o9AvE

how to get started:

1) copy over the contents of 'sharedfolder' into /srv/ . thus, it should look like '/srv/nbgrader/......'
	make sure these folders can be read and written to properly
	note: moving things into and out of 'sharedfolder' after this doens't do anything

2) 'testrepo' is the current root folder of nbgrader. name is subject to change. Other folders at the same level aren't part of the nbgrader file structure.

3) if you make updates to the original source python notebooks, do so in the folder 'Ported_exerc' and then copy over using update_source.sh or manually to the respective lesson folder in testrepo/source (a lesson may contain many .ipynb files)
	if you don't want to make changes to the exercises, ignore Ported_exerc and the .sh script

4) start a jupyter notebook instance with 'testrepo' as root for the session
	i.e. use cd to get into testrepo, then run 'jupyter notebook' on the terminal. Or use another launch method.

5) click the formgrader tab to get started. publish some exercises using the UI buttons.

6) Now pretend to be a student and retrieve the exercise from the web and fill it in and submit it. Use receive_exercises.sh or handin_exercise_students_solution.sh
Alternatively: copy them manually from /srv/nbgrader/exchange/testrepo/outbound/<LessonID> to the folder StudentWorkspace 
 and copy them manually from StudentWorkspace to /srv/nbgrader/exchange/testrepo/inbound/<studentID>/<LessonID>
	note: in order to fill in your solution, you will have to open a second jupyter notebook instance from the terminal from within StudentWorkspace
	this procedure is here to emulate uploading the student's solution to gitlab and you fetching it to grade it.


7) now go back to your jupyter UI you had opened from testrepo and refresh. under submitted there should be a non-zero number. click it to see how many students have handed something in (should be 1) and then click the UI button to autograde the solution.

8) click on the manual grading tab on the left to enter comments under the student's code or assign partial credits if they failed to pass all assert statements but their code was good.

9) use 'nbgrader feedback <LessonID>' from the terminal to generate html files with your given feedback for distribution.
